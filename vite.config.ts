import { defineConfig } from "vite"
import Vue from "@vitejs/plugin-vue"
import ViteComponents from "vite-plugin-components"
import ViteIcons, { ViteIconsResolver } from "vite-plugin-icons"
import PurgeIcons from "vite-plugin-purge-icons"
import WindiCSS from "vite-plugin-windicss"
import mix, { vercelAdapter } from "vite-plugin-mix"
import { resolve } from "path"

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      "~/": `${resolve(__dirname, "src")}/`,
    },
  },
  define: {
    "process.env": process.env,
  },
  plugins: [
    Vue({
      include: [/\.vue$/],
    }),

    // https://github.com/antfu/vite-plugin-components
    ViteComponents({
      extensions: ["vue"],
      customComponentResolvers: [
        ViteIconsResolver({
          componentPrefix: "",
        }),
      ],
    }),

    // https://github.com/antfu/vite-plugin-icons
    ViteIcons(),

    // https://github.com/windicss/vite-plugin-windicss
    WindiCSS(),

    // https://github.com/antfu/purge-icons/tree/main/packages/vite-plugin-purge-icons
    PurgeIcons(),

    // https://github.com/egoist/vite-plugin-mix
    mix({
      handler: "./api/index.ts",
      adapter: vercelAdapter(),
    }),
  ],
  optimizeDeps: {
    include: ["vue", "@vueuse/core"],
  },
})
