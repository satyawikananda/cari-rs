import { ViteSSG } from "vite-ssg/single-page"
import App from "./App.vue"
import "virtual:windi.css"
import "@purge-icons/generated"
import "~/assets/styles/style.scss"

// https://github.com/antfu/vite-ssg
export const createApp = ViteSSG(App)
