import { CariRS } from "carirs"

export const getProvinces = () => {
  const cariRS = new CariRS()
  const data = cariRS.getProvinces()
  return data
}

export const getCities = (provinceId: string) => {
  const cariRS = new CariRS()
  const data = cariRS.getCities(provinceId)
  return data
}
