import { Handler } from "vite-plugin-mix"
import { getProvinces, getCities } from "./data"

type PropId = {
  propId: string
}
export const handler: Handler = async (req, res, next) => {
  if (req.path === "/api/getProvinces") {
    const data = await getProvinces()
    res.end(JSON.stringify(data.provinces))
  }

  if (req.path === "/api/getCities") {
    const { propId }: Partial<PropId> = req.query
    const data = await getCities(propId as string)
    res.end(JSON.stringify(data.cities))
  }
  next()
}
